﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oefening_23._3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnToevoegen_Click(object sender, RoutedEventArgs e)
        {
            string result = "";
            string voornaam = "";
            string achternaam = "";
            double verdiensten = 0;

            //Achternaam check
            if (txtAchternaam.Text != null && txtAchternaam.Text != string.Empty)
            {
                if (!(double.TryParse((txtAchternaam.Text.Replace(".",",")), out double test))) 
                {
                    achternaam = txtAchternaam.Text;
                }
                else
                {
                    MessageBox.Show("Het veld achternaam kan niet numeriek zijn!", "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Het veld achternaam kan niet leeg zijn!", "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            //Voornaam check
            if (txtVoornaam.Text != null && txtVoornaam.Text != string.Empty)
            {
                if (!(double.TryParse((txtVoornaam.Text.Replace(".", ",")), out double test)))
                {
                    voornaam = txtVoornaam.Text;
                }
                else
                {
                    MessageBox.Show("Het veld voornaam kan niet numeriek zijn!", "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Het veld voornaam kan niet leeg zijn!", "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            //Verdiensten check
            if (txtVerdiensten.Text != null && txtVerdiensten.Text != string.Empty)
            {
                if (double.TryParse((txtVerdiensten.Text.Replace(".", ",")), out double test1))
                {
                    verdiensten = Convert.ToDouble(txtVerdiensten.Text.Replace(".",","));
                }
                else
                {
                    MessageBox.Show("Het veld verdiensten moet numeriek zijn!", "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Het veld vertdiensten kan niet leeg zijn!", "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            //Als alles correct werd ingegeven
            if (achternaam != null && achternaam != string.Empty && voornaam != null && voornaam != string.Empty && verdiensten != 0) 
            {
                result = achternaam.PadRight(40) + voornaam.PadRight(40) + Math.Round(verdiensten, 2).ToString("0.00") + " \u20AC" + Environment.NewLine;
                txtInhoud.Text += result;
            }

            result = "";
        }
    }
}
